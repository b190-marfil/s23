//Create trainer object
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock","Misty"]
	},
	//ADD A PARAMETER (0 TO 3) WHEN CALLING trainer.talk()
	talk: function(chosenPokemon){
		let pokemon = trainer.pokemon[chosenPokemon];
		console.log(pokemon + "! I choose you!");
	}
}

//Access the trainer object properties using dot and square bracket notation.
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

// Invoke/call the trainer talk object method.
trainer.talk(0);

// Create a constructor for creating a pokemon.
function Pokemon(name, level){
	/*Properties*/
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = 2 * level;

	/*Methods*/
	this.tackle = function(target){
		 console.log(this.name + " tackled " + target.name);
		 target.health = target.health - this.attack;
		 if(target.health <= 0){
		 	target.health = 0;
		 }
		 console.log(target.name + "'s health is now reduced to " + target.health);
		 //  Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
		 if(target.health <= 0){
		 	target.faint();
		 }
	};
	this.faint = function(){
		console.log(this.name + " fainted.");
	};
}

// Create/instantiate several pokemon object from the constructor.
let pidgey = new Pokemon("Pidgey",5);
let arbok = new Pokemon("Arbok",7);
let vulpix = new Pokemon("Vulpix",11);

console.log(pidgey);
console.log(arbok);
console.log(vulpix);

// Invoke the tackle method of one pokemon object to see if it works as intended.
pidgey.tackle(arbok);
console.log(arbok);
vulpix.tackle(arbok);
console.log(arbok);
